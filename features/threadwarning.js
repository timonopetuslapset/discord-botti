var env = require('node-env-file');
//Rodenttuksen discord author id
const author_id = process.env.WARNED_USER;


module.exports = function treadWarning(client){
	client.on('message', message => {
		
		if((-1 !==[author_id].indexOf(message.author.id))&& message.content.match(/(ylilauta|4chan)+(\.|:|\/|\w)+/g)){
			message.channel.send(process.env.THREAD_WARNING_MESSAGE).then(function(){
				message.channel.lastMessage.react('🙅');
				message.channel.lastMessage.react('❕');
			});
		}
		
	});
	return client;
};