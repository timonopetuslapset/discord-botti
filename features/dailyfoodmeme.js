//Require modules
var fs = require('fs'),
    request = require('request');
var MongoClient = require('mongodb').MongoClient

const Discord = require('discord.js');

var date = new Date().getDate();

var weekday = ["Sunday", "Monday","Tuesday","Wednesday","Thursday","Friday","Saturday" ];

const channel_id = process.env.CHANNEL_ID;

function isFriday()
{
	var d = new Date();
	if(d.getDay() == 5)
		return true;
	else
		return false;
} 

function getFoodMeme(client){
	//Assign mongodb connection
	var url = process.env.MONGO_URL;
	MongoClient.connect(url, function(err, db) {
		db.db('discord_bot').collection("foodmemes").find({friday:isFriday()}).toArray(function(err, results){
	    if (err) throw err;
	    var item = results[Math.floor(Math.random()*results.length)];
	    downloadFile(item.url, 'foodmeme.jpg', function(){
		  client.channels.forEach(function(channel){
				if(channel.id == channel_id){
					let d = new Date();
					channel.send(d.getDate() + '.' + (d.getMonth() + 1) + '.' + d.getFullYear() + ' ' + weekday[d.getDay()] + "'s daily food meme. Provided by Vaasa's memecenter.",new Discord.Attachment('foodmeme.jpg','dailyfoodmeme.jpg'));
				}
			});
		});
	    db.close();
	  });
	});
	
}

/*
	Download file from url
*/
var downloadFile = function(uri, filename, callback){
	  request.head(uri, function(err, res, body){
	    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
	 });
};

module.exports = function (client){
	getFoodMeme(client);
};