var events = require('events');
var eventEmitter = new events.EventEmitter();

module.exports = function mainLoop(client){
 	return new DiscordBot(client);
}; 

class DiscordBot{
	constructor(client) {
	    this.client = client;
	    this.date = new Date().getDate();
	    this.eventEmitter = eventEmitter;
	  }

	mainLoop() {
	    setTimeout(() => {
	    	if(this.date !== new Date().getDate()){
	    	 	this.eventEmitter.emit('datechanged');
	    	 	this.date = new Date().getDate();
	    	 }
	        this.mainLoop();
	    }, 10000)
	}
}; 

