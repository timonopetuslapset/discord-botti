//Modules
var MongoClient = require('mongodb').MongoClient;

//Regex to match emojis in the messages
const customEmojieRegex = /<:*\w*:*\d*>/g;
const emojieRegex = /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/g;

//Exports
module.exports.logMessages = function logMessages(msg){
    logMessageEmojis(msg);
};

//Exports
module.exports.logReactions = function logReactions(reaction, user){
    //Log reactions
    logReactionEmojis(reaction, user);
};

function logReactionEmojis(reaction, user){
    //Format inserts array
    const inserts = [];
    let emoji;

    /**
     * Check if the emoji is custom or not.
     * 
     * If not => format the emoji correctly
     */
    if(reaction.emoji.id !== null)
        emoji = '<:' + reaction.emoji.identifier + '>';
    else
        emoji = reaction.emoji.name;  

    //Add reaction to inserts
    inserts.push({emoji: emoji, date: new Date(), custom: false, message: false, user: user.id});
    
    //Insert to mongo!
    insertToMongo(inserts);
}

function logMessageEmojis(msg){
    //Find all emojis from the message
    const emojis = msg.content.match(emojieRegex);
    const customEmojis = msg.content.match(customEmojieRegex);
    const inserts = [];
    //If emojis are found, log them.
    if(emojis || customEmojis){
        //Normal emojis
        if(emojis){
            emojis.forEach(element => {
                inserts.push({emoji: element, date: new Date(), custom: false, message: true, user: msg.author.id});
            });
        }
        //Custom emojis
        if(customEmojis){
            customEmojis.forEach(element => {
                inserts.push({emoji: element, date: new Date(), custom: true, message: true, user: msg.author.id});
            });
        }

        insertToMongo(inserts);
        
    }
}
function insertToMongo(inserts){
    //Add the emojis to database
    MongoClient.connect(process.env.MONGO_URL, function(err, db) {
        if (err) throw err;
        //Add freshest food memes to the collection
        db.db('discord_bot').collection('emoji_usage').insertMany(inserts, function(err, res) {
            if (err) throw err;
            db.close();
        });
    });
}