//Modules
var MongoClient = require('mongodb').MongoClient;

//Exports
module.exports.commands = {
    '!topemojis': 'Top 5 of all emojis',
    '!topcustomemojis': 'Top 5 of custom emojis',
    '!topemojiusers': 'Top 5 of emoji users'
};

module.exports.analytics = function analytics(msg, client){

    //Check user input
    switch (msg.content) {
        case '!topemojis':
            emojiAnalytics(msg);
            break;
        case '!topcustomemojis':
            customEmojiAnalytics(msg);
            break;
        case '!topemojiusers':
            topEmojiUserAnalytics(msg, client);
            break;
        default:
            break;
    }
};

function emojiAnalytics(msg){
    //Response message for the user
    let response = "All Emojis Top 5:\n";
    //Aggregate
    const agr = [
        {
            $group: { _id: "$emoji", total: {$sum: 1}}
        }
    ];
    //Migrate database
    MongoClient.connect(process.env.MONGO_URL, function(err, db) {
        if (err) throw err;
        //Add freshest food memes to the collection
        db.db('discord_bot').collection("emoji_usage").aggregate(agr).toArray( (err, res) => {
            if (err) throw err;
            res.sort(function(a, b) { 
                return b.total - a.total;
            })
            //Only show top 5
            res = res.slice(0, 5);

            //Format the response.
            for (let index = 0; index < res.length; index++) {
                response += (index + 1) + ': ' + res[index]._id + ' count: ' + res[index].total + "\n";
            }
            msg.reply(response);
            db.close();
        });
    });
}

function customEmojiAnalytics(msg){
    //Response message for the user
    let response = "Custom Emojis Top 5:\n";
    //Aggregate
    const agr = [
        {
            $match : { custom : true }, 
        },
        {
            $group: { _id: "$emoji", total: {$sum: 1}}
        }
    ];
    //Migrate database
    MongoClient.connect(process.env.MONGO_URL, function(err, db) {
        if (err) throw err;
        //Add freshest food memes to the collection
        db.db('discord_bot').collection("emoji_usage").aggregate(agr).toArray( (err, res) => {
            if (err) throw err;
            res.sort(function(a, b) { 
                return b.total - a.total;
            })
            //Only show top 5
            res = res.slice(0, 5);

            //Format the response.
            for (let index = 0; index < res.length; index++) {
                response += (index + 1) + ': ' + res[index]._id + ' count: ' + res[index].total + "\n";
            }
            msg.reply(response);
            db.close();
        });
    });
}

function topEmojiUserAnalytics(msg, client){
    //Response message for the user
    let response = "All Emojis Top 5:\n";
    //Aggregate
    const agr = [
        {
            $group: { _id: "$user", total: {$sum: 1}}
        }
    ];
    //Migrate database
    MongoClient.connect(process.env.MONGO_URL, function(err, db) {
        if (err) throw err;
        //Add freshest food memes to the collection
        db.db('discord_bot').collection("emoji_usage").aggregate(agr).toArray( (err, res) => {
            if (err) throw err;
            res.sort(function(a, b) { 
                return b.total - a.total;
            })
            //Only show top 5
            res = res.slice(0, 5);

            let promiseAll = [];

            //Format the response.
            for (let index = 0; index < res.length; index++) {
                promiseAll.push(client.fetchUser(res[index]._id).then((user) => {
                    response += (index + 1) + ': ' + user.username + ' count: ' + res[index].total + "\n";
                }));
            }
            Promise.all(promiseAll).then(()=>{
                msg.reply(response);
                db.close();
            })
        
        });
    });
}