FROM node:8

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

ENV SECRET MjQyNzc3Mzk1MTkzNDQ2NDAw.XKeUgg.SinuRcj6jQJL5GMN0DFWMjg7WlE
ENV CHANNEL_ID 152436994293760010
ENV MONGO_URL mongodb://mongodb:27017/
ENV THREAD_WARNING_MESSAGE Varoitus! Rodentuksen lautalinkki!
ENV WARNED_USER 218440418306555904

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8080

CMD [ "node", "main.js" ]

