/*
	Discord bot for #rakkaudenkanaali
	Mikael Wahlfors, 2017
	wahlfors.mikael@gmail.com
*/

//Require modules
//var env = require('node-env-file');
const treadWarning = require('./features/threadwarning');
const foodMeme = require('./features/dailyfoodmeme');
const logger = require('./features/logger');
const analytics = require('./features/analytics');
const helper = require('./features/helper');
const mainLoop = require('./features/mainloop');
const Discord = require('discord.js');
const migrate = require('./database/migrations')
const env = require('node-env-file');
const fs = require('fs')

const commands = [];

/**
 * ToDo
 * Make command parser
 */
//Get all commands
for (var key in analytics.commands) {
	commands[key] = analytics.commands[key];
};
for (var key in helper.commands) {
	commands[key] = helper.commands[key];
};

//Load env file and fetch bot token. (If using docker, don't fetch .env file)
if (fs.existsSync(__dirname + '/.env')) {
	console.log('Reading .env');
    env(__dirname + '/.env');
}

//Migrate database if needed
setTimeout(migrate, 10000);

// Create an instance of a Discord client
const client = new Discord.Client();
const token = process.env.SECRET;

//Include needed functionalities
treadWarning(client);

// Log our bot in and start it
client.login(token).then(() => {
	// Start program main loop, for non event based functionalities.
	app = mainLoop();

	//Set event handlers
	app.eventEmitter.on('datechanged', () => {
		foodMeme(client);
	});

	//Log reaction emojies
	client.on('messageReactionAdd', (reaction, user) => {
		logger.logReactions(reaction, user);
	});

	//Register raw data helper
	helper.rawDataHelper(client);

	client.on("message", (msg) => {
		if(!msg.author.bot){
			logger.logMessages(msg);
		}
		if(analytics.commands[msg.content]!==undefined){
			analytics.analytics(msg, client);
		} else if(helper.commands[msg.content]!==undefined){
			helper.helper(msg, commands);
		} else {

		}
	});

	app.mainLoop(client);
	console.log('Running');
});

