//Require modules
var MongoClient = require('mongodb').MongoClient;

module.exports = function migrate(){
  //Assign mongoDB connection
  var url = process.env.MONGO_URL;

  //List of fresh food memes.
  var myobj = [
      { id: 1, url: 'https://i.imgur.com/z1qT09i.jpg', friday: false},
      { id: 2, url: 'https://i.imgur.com/ey7vsli.jpg', friday: false},
      { id: 3, url: 'https://i.imgur.com/618zjqL.jpg', friday: false},
      { id: 4, url: 'https://i.imgur.com/PgE8DlM.jpg', friday: false},
      { id: 5, url: 'https://i.imgur.com/HHFV7kr.jpg', friday: true},
      { id: 6, url: 'https://i.imgur.com/1psyAfi.jpg', friday: false},
      { id: 7, url: 'https://i.imgur.com/rOuO3o0.jpg', friday: false},
      { id: 8, url: 'https://i.imgur.com/7xxB3Hw.jpg', friday: false},
      { id: 9, url: 'https://i.imgur.com/9vKWXD2.jpg', friday: false},
      { id: 10, url: 'https://i.imgur.com/pQPYT7U.jpg', friday: false},
      { id: 11, url: 'https://i.imgur.com/tup3FII.jpg', friday: false},
      { id: 12, url: 'https://i.imgur.com/FJQwLmg.jpg', friday: true},
      { id: 13, url: 'https://i.imgur.com/w90DIcZ.jpg', friday: false},
      { id: 14, url: 'https://i.imgur.com/nSWDmWV.jpg', friday: false},
      { id: 15, url: 'https://i.imgur.com/gQ8vTyM.jpg', friday: false},
      { id: 16, url: 'https://i.imgur.com/Hm02UQF.jpg', friday: false},
      { id: 17, url: 'https://i.imgur.com/AnwAifX.jpg', friday: false},
      { id: 18, url: 'https://i.imgur.com/Dor0jo1.jpg', friday: false},
      { id: 19, url: 'https://i.imgur.com/VdcwQUw.jpg', friday: false},
      { id: 20, url: 'https://i.imgur.com/F5qNF3z.jpg', friday: false},
      { id: 21, url: 'https://i.imgur.com/dY3jnZh.jpg', friday: false},
      { id: 22, url: 'https://i.imgur.com/XUjsSXz.png', friday: false},
      { id: 23, url: 'https://i.imgur.com/rjWYODE.jpg', friday: false},
      { id: 24, url: 'https://i.imgur.com/ZZE2zy3.jpg', friday: false},
      { id: 25, url: 'https://i.imgur.com/mkDarUC.png', friday: false},
      { id: 26, url: 'https://i.imgur.com/eMVGKQw.jpg', friday: false},
      { id: 27, url: 'https://i.imgur.com/juhJGep.jpg', friday: false},
      { id: 28, url: 'https://i.imgur.com/OeBEzJJ.jpg', friday: false},
      { id: 29, url: 'https://i.imgur.com/o2quiAf.jpg', friday: false},
      { id: 30, url: 'https://i.imgur.com/FDdAkaF.jpg', friday: false},
      { id: 31, url: 'https://i.imgur.com/9GcRkCk.jpg', friday: false},
      { id: 32, url: 'https://i.imgur.com/JVxYXD2.jpg', friday: false},
      { id: 33, url: 'https://i.imgur.com/DiI92FE.jpg', friday: false},
      { id: 34, url: 'https://i.imgur.com/G9P24p1.jpg', friday: false},
      { id: 35, url: 'https://i.imgur.com/dlvpFve.png', friday: false},
      { id: 36, url: 'https://i.imgur.com/jNyCXbL.jpg', friday: false},
      { id: 37, url: 'https://i.imgur.com/cqhZuzo.jpg', friday: false},
      { id: 38, url: 'https://i.imgur.com/nO0hL2R.png', friday: false},
      { id: 39, url: 'https://i.imgur.com/SWrJQEy.jpg', friday: false},
      { id: 40, url: 'https://i.imgur.com/oTcWVlc.jpg', friday: true},
      { id: 41, url: 'https://i.imgur.com/ZzIloDD.jpg', friday: true},
      { id: 42, url: 'https://i.imgur.com/hBqbq7Y.jpg', friday: false},
      { id: 43, url: 'https://i.imgur.com/Hc0P7sN.jpg', friday: true}
    ];
    
  //Drop collection
  MongoClient.connect(url, function(err, db) {
    if(err) throw err;   
    db.db('discord_bot').collection("foodmemes").drop(function(err, delOK) {
      if (err) migrate();
      if (delOK) console.log("Collection dropped");
      migrate();
      db.close();
    });
    }
  );

  //Migrate database
  var migrate = function(){ MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    //Add freshest food memes to the collection
    db.db('discord_bot').collection('foodmemes').insertMany(myobj, function(err, res) {
      if (err) throw err;
      console.log('discord_bot database created');
      console.log('Number of entries entered to foodmemes table');
      db.close();
    });

  })};
}; 

